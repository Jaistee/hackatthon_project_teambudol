<?php 
	require "../templates/template.php";
	function get_content(){
	require "../controllers/connection.php";
 ?>

 	<h1 class="text-center py-5">YOUR DASHBOARD</h1>
 	<div class="container">
 			<h6>Categories</h6>
	 			<ul class="list-group-border">
	 				<li class="list-group-item">
	 					<a href="index.php">All</a>
	 				</li>
	 		<?php
	 				//CALL CATEGORIES
	 				$categories_query = "SELECT * FROM categories";
	 				$categoryList = mysqli_query($conn, $categories_query);
	 				
	 				foreach ($categoryList as $indiv_category){
	 		?>
	 				<li class="list-group-item">
	 				 	<a href="index.php?category_id=<?php echo$indiv_category['id']?>"><?php echo $indiv_category['Name']?></a>
	 				</li>
	 		<?php 
	 				}
	 		?>
	 			</ul>	
 	<div class="row">
 		<div class="col-lg-4">

	 			<!-- User profile -->
	 		<h3>user profile</h3>
	 		<?php 
	 			
	 		$users_query = "SELECT * FROM users"; 
	 		$users = mysqli_query($conn, $users_query);
	 			

	 		foreach ($users as $indiv_user) {
	 		?>
	 			<div class="">
	 				<div class="card">
	 					<form action="../controllers/process_add_salary.php" method="POST">
	 						<?php 
	 						$userId = $indiv_user['id'];

	 						$userId_query = "SELECT * FROM users WHERE id = $userId";
	 						$user = mysqli_fetch_assoc(mysqli_query($conn, $userId_query));
	 						
	 						?>					
	 					<div class="card-body">	
	 						<p class="card-text"><?php echo $indiv_user['email']?></p>
	 						<p class="card-text"><?php echo $indiv_user['username']?></p>
	 						<p class="card-text"><?php echo $indiv_user['budget']?></p>
	 						<?php 
	 							$total_budget = $indiv_user['budget']-$indiv_expense['expensePrice'];
	 							
	 						 ?>
	 						<p class="card-text"><?php echo $indiv_user['$remaining']?></p>
	 						<input type="hidden" name="id" value="<?php echo $indiv_user['id']?>">
	 						<input type="number" name="salary" placeholder="enter savings" value>
	 					<div class="card-body">	
	 						<button class="btn btn-success" type="submit">SAVE SALARY</button>
	 						</div>
	 					</div>
	 					</form>
	 				</div>
	 			</div>
	 				
	 			<!-- sidebar -->
	 		
	 	</div>	
 		<!-- Item list -->
 		<div class="col-lg-8">
 		<h1>Expenses</h1>
	 		<?php  			
	 		//mysqli ($connection variable from connection.php, query)

	 		$expense_query = "SELECT * FROM expenses";
	 			// for filtering 
	 		if(isset($_GET['category_id'])){
	 			//concatenate . = " space "
	 			$catId = $_GET['category_id'];
	 			$expense_query .= " WHERE category_id = $catId";
	 		}

	 		$expense = mysqli_query($conn, $expense_query);
	 		foreach ($expense as $indiv_expense) {
	 			?>

	 			<div class="">
	 				
	 				<div class="card">
	 					
	 					<div class="card-body">
	 						<h4 class="card-title"><?php echo $indiv_expense['expenseName']?></h4>
	 						<p class="card-text">Php<?php echo $indiv_expense['expensePrice']?>.00</p>
	 						<p class="card-text"><?php echo $indiv_expense['date']?></p>
	 			<?php 
	 						$catId = $indiv_expense['category_id'];

	 						$category_query = "SELECT * FROM categories WHERE id = $catId";
	 						$category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));
	 						
	 			?>
	 						<p class="card-text">Category: <?php echo $category['Name']?></p>				
	 					</div>
	 					<!-- enter session -->
	 					<div class="card-footer">
	 						<a href="edit_item_form.php?id=<?php echo $indiv_expense['id']?>" class="btn btn-warning">Edit</a>
	 						<a href="../controllers/process_delete_item.php?id=<?php echo $indiv_expense['id']?>" class="btn btn-danger">Delete</a>
	 					</div>
	 					
	 			<?php 
	 						}
	 			?> 					
	 				</div>
	 			</div>
	 			<?php 
	 		}

 		//Steps for retrieving items
 		//1. Create a query
 		//2. Use mysqli_query to get the results
 		//3. if array (use foreach)
 		//4. if object (use mysqli_fetch_assoc to an associative array)
 		//4.1 Use foreach ($result as $key => $value)
 		
 		?>
 		</div>

 		




 	


  	</div>
 </div>
 	
 




 <?php
}
?>