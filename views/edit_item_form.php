<?php 
	require "../templates/template.php";

	function get_content(){
		require "../controllers/connection.php";
 ?>

 	<h1 class="text-center py-5">EDIT ITEM FORM</h1>
 	<div class="container">
 		<div class="col-lg-6 offset-lg-3">
 		<form action="../controllers/process_edit_item.php" method="POST" enctype="multipart/form-data">

 			<?php 
 				$item_id= $_GET['id'];
 				$item_query ="SELECT * FROM expenses WHERE id = $item_id";

 				$item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));
 				
 			 ?>
 			<div class="form-group">
 				<label for="name">Item Name:</label>
 				<input type="text" name="name" class="form-control" value="<?php echo $item['expenseName']?>">
 			</div>
 			<div class="form-group">
 				<label for="price">Item Price:</label>
 				<input type="number" name="price" class="form-control" value="<?php echo $item['expensePrice']?>">
 			</div>
 			
 			
 			<div class="form-group">
 				<label for="category_id">Item Category:</label>
 				<select name="category_id" class="form-control">
 				<!-- We'll need to get all categories from the field to be userfriendly -->
 				<?php $category_query = "SELECT * FROM categories";
 				$categories =mysqli_query($conn, $category_query);
 				foreach ($categories as $indiv_category){
 				?>
 				<option 
 				value="<?php echo $indiv_category['id']?>"
 				 	<?php echo $item['category_id'] == $indiv_category['id'] ?"selected" : "" ?>
 				><?php echo $indiv_category['Name']?></option>
 				<?php 
 				}
 				 ?></select>
 			</div>
 			<input type="hidden" value="<?php echo $item['id']?>" name="id">
 			<button class="btn btn-success" type="submit">Edit Item</button>
 		</form>
 	</div>
 </div>
 <?php 
	}
  ?>
