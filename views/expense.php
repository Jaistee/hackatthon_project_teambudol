<?php 
	require "../templates/template.php";
	function get_content(){
	require "../controllers/connection.php";
 ?>

<h1 class="text-center">ADD EXPENSE FORM</h1>
 	<div class="container">
 		<div class="">
 		<form action="../controllers/process_add_item.php" method="POST">
 			<div class="form-group">
 				<label for="name">Expense Name:</label>
 				<input type="text" name="name" class="form-control">
 			</div>
 			<div class="form-group">
 				<label for="price">Expense Value:</label>
 				<input type="number" name="price" class="form-control">
 			</div>
 			
 			<div class="form-group">
 				<label for="category_id">Item Category:</label>
 				<select name="category_id" class="form-control">
 				<!-- We'll need to get all categories from the field to be userfriendly -->
 				<?php $category_query = "SELECT * FROM categories";
 				$categories = mysqli_query($conn, $category_query);
 				foreach ($categories as $indiv_category){
 				?>
 				<option value="<?php echo $indiv_category['id'] ?>"><?php echo $indiv_category['Name'] ?></option>
 				<?php 
 				}
 				 ?></select>
 			</div>
 			<button class="btn btn-success" type="submit">Add Expense</button>
 		</form>
 	</div>
 </div>
 <?php 
}
  ?>
