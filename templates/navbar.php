<!DOCTYPE html>
<html>
<head>
	<title>	</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/litera/bootstrap.css">
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor02">
    <ul class="navbar-nav mr-auto">
      
   
<!-- session -->
<?php
require "../controllers/connection.php";
        session_start();
          if(isset($_SESSION['user'])){
      
?>
      <li class="nav-item">
        <a class="nav-link" href="../controllers/process_logout.php">Logout</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="../views/expense.php">ADD EXPENSE</a>
      </li>


<?php
}else{
	?>
<!-- else -->

  <li class="nav-item">
        <a class="nav-link" href="../views/register.php">Register</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../views/login.php">Login</a>
      </li>
<?php 
}

?>

    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search">
      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>

</body>
</html>